/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * MainWindow.h
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

#include <gtkmm.h>
#include <iostream>
#include <string>

#include "GameBox.h"


class MainWindow : public Gtk::ApplicationWindow
{
protected:
	enum Difficulty {EASY, MEDIUM, HARD, CUSTOM};
	
public:
	MainWindow(BaseObjectType* cobject, 
			   const Glib::RefPtr<Gtk::Builder>& refBuilder);
	
	static MainWindow* create();

protected:
	Glib::RefPtr<Gtk::Builder> m_refBuilder;
	void on_realize() override;
	
	Gtk::Button 		*m_pNewGameButton; 
	Gtk::Button 		*m_pEasyButton, *m_pMediumButton, 
						*m_pHardButton, *m_pCustomOK;
	Gtk::Entry  		*m_pRowsEntry, *m_pColumnsEntry, *m_pMinesEntry;
	Gtk::PopoverMenu 	*m_pMainPopoverMenu;
	Gtk::Revealer 		*m_pOKRevealer;
	
	void on_NewGameButton_clicked();
	bool on_NewGameButton_pressed(GdkEventButton* event);
	void on_MenuButton_clicked(Difficulty difficulty);
	void on_CustomEntry_changed(Gtk::Entry* active_entry);
	
private:
	int  m_rows, m_columns, m_mines;
};

#endif // _MAINWINDOW_H_

/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * MainWindow.cc
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "MainWindow.h"

MainWindow::MainWindow	(BaseObjectType* cobject,
					   	 const Glib::RefPtr<Gtk::Builder>& refBuilder)
:Gtk::
 ApplicationWindow		(cobject)
,m_refBuilder			(refBuilder)
,m_pNewGameButton		(nullptr)
,m_pEasyButton			(nullptr)
,m_pMediumButton		(nullptr)
,m_pHardButton			(nullptr)
,m_pCustomOK			(nullptr)
,m_pRowsEntry			(nullptr)
,m_pColumnsEntry		(nullptr)
,m_pMinesEntry			(nullptr)
,m_pMainPopoverMenu		(nullptr)
,m_pOKRevealer			(nullptr)
,m_rows					(9)
,m_columns				(9)
,m_mines				(10)
{
	m_refBuilder->get_widget("NewGameButton", m_pNewGameButton);
	if (!m_pNewGameButton)
		throw std::runtime_error("No \"NewGameButton\" object " 
								 "in MainWindow.glade");
		
	m_refBuilder->get_widget("MainPopoverMenu", m_pMainPopoverMenu);
	if (!m_pMainPopoverMenu)
		throw std::runtime_error("No \"MainPopoverMenu\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("EasyButton", m_pEasyButton);
	if (!m_pEasyButton)
		throw std::runtime_error("No \"EasyButton\" object " 
								 "in MainWindow.glade");
								
	m_refBuilder->get_widget("MediumButton", m_pMediumButton);
	if (!m_pMediumButton)
		throw std::runtime_error("No \"MediumButton\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("HardButton", m_pHardButton);
	if (!m_pHardButton)
		throw std::runtime_error("No \"HardButton\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("OKCustom", m_pCustomOK);
	if (!m_pCustomOK)
		throw std::runtime_error("No \"OKCustom\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("OKRevealer", m_pOKRevealer);
	if (!m_pOKRevealer)
		throw std::runtime_error("No \"OKRevealer\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("RowsEntry", m_pRowsEntry);
	if (!m_pRowsEntry)
		throw std::runtime_error("No \"RowsEntry\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("ColumnsEntry", m_pColumnsEntry);
	if (!m_pColumnsEntry)
		throw std::runtime_error("No \"ColumnsEntry\" object " 
								 "in MainWindow.glade");
								 
	m_refBuilder->get_widget("MinesEntry", m_pMinesEntry);
	if (!m_pMinesEntry)
		throw std::runtime_error("No \"MinesEntry\" object " 
								 "in MainWindow.glade");					 							 														 
}


//static
MainWindow* 
MainWindow::create()
{
	auto refBuilder = Gtk::Builder::
					  create_from_resource(
					  "/MineSweeper/../gui/MainWindow.glade");

	MainWindow* pMainWindow = nullptr;
	refBuilder->get_widget_derived("MainWindow", pMainWindow);
	if (!pMainWindow)
		throw std::runtime_error("No \"MainWindow\" object " 
								 "in MainWindow.glade");
	
	return pMainWindow;
}


void 
MainWindow::on_realize()
{ 
	Gtk::ApplicationWindow::on_realize();
	
	// Initialize Game in Easy-Mode.
	try
  	{
    	auto pGameBox = GameBox::create(m_rows, m_columns, m_mines);
		add(*pGameBox);
  	}
  	catch(const Glib::Error& ex)
  	{
    	std::cerr << "MainWindow::on_realize(): " << ex.what() << std::endl;
  	}
  	catch(const std::exception& ex)
  	{
    	std::cerr << "MainWindow::on_realize(): " << ex.what() << std::endl;
  	}

  	// Connect NewGame-Menu-Button signals.
	m_pNewGameButton->signal_clicked().
	connect(sigc::mem_fun(*this, &MainWindow::on_NewGameButton_clicked));
	
	m_pNewGameButton->signal_button_press_event().
	connect(sigc::mem_fun(*this, &MainWindow::on_NewGameButton_pressed));
	
	m_pEasyButton->signal_clicked().
	connect(sigc::bind <Difficulty>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_MenuButton_clicked), EASY));
	
	m_pMediumButton->signal_clicked().
	connect(sigc::bind <Difficulty>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_MenuButton_clicked), MEDIUM));
	
	m_pHardButton->signal_clicked().
	connect(sigc::bind <Difficulty>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_MenuButton_clicked), HARD));
	
	m_pCustomOK->signal_clicked().
	connect(sigc::bind <Difficulty>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_MenuButton_clicked), CUSTOM));
	
	// Connect Custom-Entry signals.
	m_pRowsEntry->signal_changed().
	connect(sigc::bind <Gtk::Entry*>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_CustomEntry_changed), m_pRowsEntry));
	
	m_pColumnsEntry->signal_changed().
	connect(sigc::bind <Gtk::Entry*>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_CustomEntry_changed), m_pColumnsEntry));
	
	m_pMinesEntry->signal_changed().
	connect(sigc::bind <Gtk::Entry*>
		   (sigc::mem_fun
		   (*this, &MainWindow::on_CustomEntry_changed), m_pMinesEntry));
}


void 
MainWindow::on_NewGameButton_clicked()
{
	m_pMainPopoverMenu->show();
}


bool 
MainWindow::on_NewGameButton_pressed(GdkEventButton* event)
{
	// Only react to middle-mouse Button click.
	if(event->button == 2)
	{
		remove();
		add(*GameBox::create(m_rows, m_columns, m_mines));
	}
	
	return false;
}


void 
MainWindow::on_MenuButton_clicked(Difficulty difficulty)
{
	remove();
	
	// Input check for custom in "MainWindow::on_CustomEntry_changed(...)".
	// Save in member variables for "middle-click"-Restart option.
	switch(difficulty)
	{
		case EASY	: m_rows =  9; m_columns =  9; m_mines = 10;	break;
		case MEDIUM	: m_rows = 16; m_columns = 16; m_mines = 40;	break;
		case HARD	: m_rows = 16; m_columns = 30; m_mines = 99;	break;
		case CUSTOM	: m_rows 	= std::stoi(m_pRowsEntry	->get_text());
					  m_columns = std::stoi(m_pColumnsEntry	->get_text());
					  m_mines 	= std::stoi(m_pMinesEntry	->get_text()); 
					  break;
		default		: break;
	}
	add(*GameBox::create(m_rows, m_columns, m_mines));
}


void 
MainWindow::on_CustomEntry_changed(Gtk::Entry* active_entry)
{
	// Catch non-number inputs and erase them.
	auto text = active_entry->get_text();
	for(unsigned int i = 0; i < text.length(); i++)
	{
		if(!Glib::Unicode::isdigit(text[i])) text.erase(i);
	}
	active_entry->set_text(text);
	
	// Check whether OK-Button should be sensitive and revealed.
	bool ready = false;
	if(m_pRowsEntry->get_text().length()	> 0
	&& m_pColumnsEntry->get_text().length()	> 0
	&& m_pMinesEntry->get_text().length()	> 0)
	{
		int input_rows		= std::stoi(m_pRowsEntry	->get_text());
		int input_columns	= std::stoi(m_pColumnsEntry	->get_text());
		int input_mines		= std::stoi(m_pMinesEntry	->get_text());
		
		if((input_rows 		>=  9 && input_rows 	<= 24)
		&& (input_columns 	>=  9 && input_columns 	<= 30)
		&& (input_mines 	>= 10 && input_mines 	<= (input_rows - 1) * 
													   (input_columns - 1)))
		{
			ready = true;
		}
	}
	m_pOKRevealer->set_reveal_child(ready);
	m_pCustomOK->set_sensitive(ready);
}


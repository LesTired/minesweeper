/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * FieldButton.cc
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "FieldButton.h"

FieldButton::FieldButton(BaseObjectType* cobject,
					   	 const Glib::RefPtr<Gtk::Builder>& refBuilder)
:Gtk::ToggleButton		(cobject)
,m_refBuilder			(refBuilder)
,m_pButtonSVG			(nullptr)
,m_is_flagged			(false)
{
	m_refBuilder->get_widget("ButtonSVG", m_pButtonSVG);
	if (!m_pButtonSVG)
		throw std::runtime_error("No \"ButtonSVG\" object " 
								 "in FieldButton.glade");
}


//static
FieldButton* 
FieldButton::create(bool is_bomb)
{
	// Load the Builder file and instantiate its widgets.
	auto refBuilder = Gtk::Builder::
					  create_from_resource(
					  "/MineSweeper/../gui/FieldButton.glade");

	FieldButton* pFieldButton = nullptr;
	refBuilder->get_widget_derived("FieldButton", pFieldButton);
	if (!pFieldButton)
		throw std::runtime_error("No \"FieldButton\" object "
								 "in FieldButton.glade");
	
	pFieldButton->m_is_bomb = is_bomb;
	
	return pFieldButton;
}


bool 
FieldButton::get_is_bomb()			
{
	return m_is_bomb;		
}


void 
FieldButton::set_is_bomb(bool is_bomb)	
{
	m_is_bomb = is_bomb;		
}


bool 
FieldButton::get_is_flagged()		
{
	return m_is_flagged;	
}


void 
FieldButton::toggle_is_flagged()
{
	if(!m_is_flagged)
	{
		set_icon(FLAG);
	}
	else
	{
		set_icon(NR0);
	}	    
	
	m_is_flagged = !m_is_flagged;
}


void 
FieldButton::set_icon(IconName icon)
{
	std::string filename;
	
	switch(icon)
	{
		case NR0	: filename = "../data/nr0.svg"; 			break;
		case NR1	: filename = "../data/nr1.svg";				break;
		case NR2	: filename = "../data/nr2.svg"; 			break;
		case NR3	: filename = "../data/nr3.svg"; 			break;
		case NR4	: filename = "../data/nr4.svg"; 			break;
		case NR5	: filename = "../data/nr5.svg"; 			break;
		case NR6	: filename = "../data/nr6.svg"; 			break;
		case NR7	: filename = "../data/nr7.svg"; 			break;
		case NR8	: filename = "../data/nr8.svg"; 			break;
		case BOMB	: filename = "../data/bomb.svg"; 			break;
		case FLAG	: filename = "../data/flag.svg"; 			break;
		case EXPLODE: filename = "../data/bomb_exploded.svg"; 	break;
		case WRONG	: filename = "../data/wrong_flag.svg"; 		break;
		default		: filename = "../data/nr0.svg"; 			break;
	}
	
	m_pButtonSVG->set_from_resource("/MineSweeper/" + filename);
}

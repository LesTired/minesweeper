/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * GameBox.h
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _GAMEBOX_H_
#define _GAMEBOX_H_

#include <gtkmm.h>
#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <stdlib.h>
#include <time.h>

#include "FieldButton.h"


class GameBox : public Gtk::Box
{
public:
	GameBox(BaseObjectType* cobject, 
			const Glib::RefPtr<Gtk::Builder>& refBuilder);
	
	static GameBox* create(int rows, int columns, int mines);

protected:
	Glib::RefPtr<Gtk::Builder> m_refBuilder;
	void on_realize() override;
	void on_FieldButton_clicked(FieldButton* button);
	bool on_FieldButton_pressed(GdkEventButton* event, FieldButton* button);
	bool on_timeout();
	
private:
	std::vector<std::vector<FieldButton*>> create_FieldButtons();
	void on_hide_FieldButton(FieldButton* button);
	std::vector<std::vector<FieldButton*>> m_pFieldButtons;
	Gtk::Label *m_pTimerLabel, *m_pStateLabel;
	int  m_rows, m_columns, m_mines;
	bool m_clockruns; int m_clock;
};

#endif // _GAMEBOX_H_

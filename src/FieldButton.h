/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * FieldButton.h
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _FIELDBUTTON_H_
#define _FIELDBUTTON_H_

#include <gtkmm.h>
#include <string>
#include <iostream>

enum IconName {NR0, NR1, NR2, NR3, NR4, NR5, NR6, NR7, NR8, 
				   BOMB, FLAG, EXPLODE, WRONG};

class FieldButton : public Gtk::ToggleButton
{			   
public:
	FieldButton(BaseObjectType* cobject, 
			   	const Glib::RefPtr<Gtk::Builder>& refBuilder);
	
	static FieldButton* create(bool is_bomb);
	bool get_is_bomb();
	void set_is_bomb(bool is_bomb);
	bool get_is_flagged();
	void toggle_is_flagged();
	void set_icon(IconName icon);
	
protected:
	Glib::RefPtr<Gtk::Builder> m_refBuilder;
	
private:
	Gtk::Image* m_pButtonSVG;
	bool m_is_bomb, m_is_flagged;
};

#endif // _FIELDBUTTON_H_

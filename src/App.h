/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * App.h
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _APP_H_
#define _APP_H_

#include <gtkmm.h>
#include <iostream>
#include <exception>

#include "MainWindow.h"


class App : public Gtk::Application
{
protected:
	App();

public:
	static Glib::RefPtr<App> create();
	
protected:
	void on_activate() override;

private:
	MainWindow* create_MainWindow();
	void on_hide_window(Gtk::Window* window);
};

#endif // _APP_H_

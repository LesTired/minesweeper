/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * GameBox.cc
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "GameBox.h"

GameBox::GameBox(BaseObjectType* cobject,
			 	 const Glib::RefPtr<Gtk::Builder>& refBuilder)
:Gtk::Box		(cobject)
,m_refBuilder	(refBuilder)
,m_pTimerLabel	(nullptr)
,m_pStateLabel	(nullptr)
,m_clockruns	(false)
,m_clock		(0)
{
	m_refBuilder->get_widget("TimerLabel", m_pTimerLabel);
	if (!m_pTimerLabel)
		throw std::runtime_error("No \"TimerLabel\" object " 
								 "in GameBox.glade");
								 
	m_refBuilder->get_widget("StateLabel", m_pStateLabel);
	if (!m_pStateLabel)
		throw std::runtime_error("No \"StateLabel\" object " 
								 "in GameBox.glade");
					 
	Glib::signal_timeout().
	connect(sigc::mem_fun(*this, &GameBox::on_timeout), 1000);
}


//static
GameBox* 
GameBox::create(int rows, int columns, int mines)
{
	auto refBuilder = Gtk::Builder::
					  create_from_resource(
					  "/MineSweeper/../gui/GameBox.glade");
		
	GameBox* pGameBox = nullptr;
	refBuilder->get_widget_derived("GameBox", pGameBox);
	if (!pGameBox)
		throw std::runtime_error("No \"GameBox\" object in GameBox.glade");

	pGameBox->m_rows	= rows;
	pGameBox->m_columns	= columns;
	pGameBox->m_mines	= mines;
		
	return pGameBox;
}


void 
GameBox::on_realize()
{
	Gtk::Box::on_realize();
	
	// Setup Buttons for MineField.
	try
  	{
    	auto pFieldButtons = create_FieldButtons();
  	}
  	catch (const Glib::Error& ex)
  	{
    	std::cerr << "GameBox::on_realize(): " << ex.what() << std::endl;
  	}
  	catch (const std::exception& ex)
  	{
    	std::cerr << "GameBox::on_realize(): " << ex.what() << std::endl;
  	}
  	
  	m_pStateLabel->set_markup("<big>0/" +  std::to_string(m_mines) + "</big>");
}


void 
GameBox::on_FieldButton_clicked(FieldButton* button)
{
	// Check whether the Button is Flagged.
	if(button->get_is_flagged())
	{
		button->set_active(false);
		return;
	}
	
	// Check whether the Button is a Bomb.
	if(button->get_is_bomb())
	{
		// Make sure that on first click there is no bomb.
		if(m_clockruns == false)
		{
			button->set_is_bomb(false);
			
			int newbomb_row;
			int newbomb_column;
			do
			{
				newbomb_row	= rand()%m_rows;
				newbomb_column = rand()%m_columns;
			} 
			while(m_pFieldButtons[newbomb_column][newbomb_row]->get_is_bomb());
			m_pFieldButtons[newbomb_column][newbomb_row]->set_is_bomb(true);
			
			on_FieldButton_clicked(button);
			return;
		}
		
		// Clicked Bomb gets special explosion-icon.
		button->set_icon(EXPLODE);
		
		// Stop timer.
		m_clockruns = false;
		
		for(int i = 0; i < m_columns; i++)
		{
			for(int j = 0; j < m_rows; j++)
			{
				// Open all other mines.
				if( m_pFieldButtons[i][j]->get_is_bomb() 
				&& !m_pFieldButtons[i][j]->get_active()
				&& !m_pFieldButtons[i][j]->get_is_flagged())
				{
					m_pFieldButtons[i][j]->set_icon(BOMB);
				}
				
				// Show all wrongly set flags.
				if(!m_pFieldButtons[i][j]->get_is_bomb() 
				&&  m_pFieldButtons[i][j]->get_is_flagged())
				{
					m_pFieldButtons[i][j]->set_icon(WRONG);
				}
				
				// Set the whole Buttonfield insensitive to further clicks.
				m_pFieldButtons[i][j]->set_sensitive(false);
			}
		}
		
		// Set state-Label to Game Over.
		m_pStateLabel->set_markup("<big><span foreground=\"darkred\">" 
								  "Game Over!</span></big>");
		return;
	}
	
	// Start the clock on first click.
	m_clockruns = true;
	
	// Get the position of the clicked Button.
	int pos_row = 0;
	int pos_column = 0;
	for(int i = 0; i < m_columns; i++)
	{
		for(int j = 0; j < m_rows; j++)
		{
			if(m_pFieldButtons[i][j] == button)
			{
				pos_column = i; 
				pos_row = j;
			}
		}
	}
	button->set_active(true);
	
	// Count bombs around the Button.
	int surrounding_bombs = 0;
	for(int i = -1; i <= 1; i++)
	{
		for(int j =- 1; j <= 1; j++)
		{
			if(pos_row + j < 0 || pos_column + i < 0
			|| pos_row + j >= m_rows || pos_column + i >= m_columns
			|| (i == 0 && j == 0))
			{
				continue;
			}
			
			if(m_pFieldButtons[pos_column + i][pos_row + j]->get_is_bomb())
			{
				surrounding_bombs++;
			}
		}
	}
	button->set_icon((IconName)surrounding_bombs);
	
	// Open all surrounding fields if there is no mine around.
	if(surrounding_bombs == 0)
	{
		for(int i = -1; i <= 1; i++)
		{
			for(int j = -1; j <= 1; j++)
			{
				if(pos_row + j < 0 || pos_column + i < 0
				|| pos_row + j >= m_rows || pos_column + i >= m_columns
				|| (i == 0 && j == 0))
				{
					continue;
				}
			
				if(!m_pFieldButtons[pos_column + i][pos_row + j]->get_active())
				{
					m_pFieldButtons[pos_column + i][pos_row + j]
					->set_active(true);
				}
			}
		}
	}
	
	// Determine if Game is already won.
	int unrevealed_buttons = 0;
	for(int i = 0; i < m_columns; i++)
	{
		for(int j = 0; j < m_rows; j++)
		{
			if(!m_pFieldButtons[i][j]->get_active())
			{
				unrevealed_buttons++;
			}
		}
	}
	
	if(unrevealed_buttons == m_mines)
	{
		// Stop timer.
		m_clockruns = false;
		
		for(int i = 0; i < m_columns; i++)
		{
			for(int j = 0; j < m_rows; j++)
			{
				if(!m_pFieldButtons[i][j]->get_is_flagged() 
				&&  m_pFieldButtons[i][j]->get_is_bomb())
				{
					m_pFieldButtons[i][j]->toggle_is_flagged();
				}
				
				m_pFieldButtons[i][j]->set_sensitive(false);
			}
		}
		
		// Set State-Label to Congratulations.
		m_pStateLabel->set_markup("<big><span foreground=\"green\">" 
								  "Congratulations!</span></big>");
	}
}


bool 
GameBox::on_FieldButton_pressed(GdkEventButton* event, FieldButton* button)
{
	// Actions on Right-click.
	if(event->button == 3 && !button->get_active()) 
	{
		button->toggle_is_flagged();
		
		// Count the amount of set flags.
		int flag_cnt = 0;
		for(int i = 0; i < m_columns; i++)
		{
			for(int j = 0; j < m_rows; j++)
			{
				if(m_pFieldButtons[i][j]->get_is_flagged())
				{
					flag_cnt++;
				}
			}
		}
		
		// Set the Flag-counter Label
		if(flag_cnt <= m_mines)
		{
			m_pStateLabel->set_markup("<big>" + 
								  	  std::to_string(flag_cnt) + "/" + 
								  	  std::to_string(m_mines) +
								  	  "</big>");
		}
		else
		{
			m_pStateLabel->set_markup("<big><span foreground=\"darkred\">" + 
									  std::to_string(flag_cnt) + 
									  "</span>/" + 
									  std::to_string(m_mines) +
									  "</big>");
		}
	}
	
	// Actions on Middle-click.
	if(event->button == 2 && button->get_active()) 
	{
		// Get the position of the clicked Button.
		int pos_row = 0;
		int pos_column = 0;
		for(int i = 0; i < m_columns; i++)
		{
			for(int j = 0; j < m_rows; j++)
			{
				if(m_pFieldButtons[i][j] == button)
				{
					pos_column = i; 
					pos_row = j;
				}
			}
		}
		
		// Count bombs and flags around the Button.
		int surrounding_bombs = 0;
		int surrounding_flags = 0;
		for(int i = -1; i <= 1; i++)
		{
			for(int j = -1; j <= 1; j++)
			{
				if(pos_row + j < 0 || pos_column + i < 0
				|| pos_row + j >= m_rows || pos_column + i >= m_columns
				|| (i == 0 && j == 0))
				{
					continue;
				}
			
				if(m_pFieldButtons[pos_column + i][pos_row + j]
				   ->get_is_bomb())
				{
					surrounding_bombs++;
				}
			
				if(m_pFieldButtons[pos_column + i][pos_row + j]
				   ->get_is_flagged())
				{
					surrounding_flags++;
				}
			}
		}
		
		// Open up surrounding fields.
		if(surrounding_bombs == surrounding_flags)
		{
			for(int i = -1; i <= 1; i++)
			{
				for(int j = -1; j <= 1; j++)
				{
					if(pos_row + j < 0 || pos_column + i < 0
					|| pos_row + j >= m_rows || pos_column + i >= m_columns
					|| (i == 0 && j == 0))
					{
						continue;
					}
			
					if(!m_pFieldButtons[pos_column + i][pos_row + j]->
						get_is_flagged())
					{
						m_pFieldButtons[pos_column + i][pos_row + j]->
						set_active(true);
					}
				}
			}
		}
	}
	
	return true;
}

std::vector<std::vector<FieldButton*>> GameBox::create_FieldButtons()
{
	// Get the Grid-widget from file.
	Gtk::Grid* pFieldGrid = nullptr;
	m_refBuilder->get_widget("FieldGrid", pFieldGrid);
	if (!pFieldGrid)
		throw std::runtime_error("No \"FieldGrid\" object in GameBox.glade");
	
	// Setup random-numbers to distribute the bombs.
	srand (time (NULL));
	std::set<int> RandomNums;
	do
	{
		RandomNums.insert(rand()%(m_rows * m_columns));
	} while(RandomNums.size() < (unsigned int) m_mines);
	
	// Fill the Grid with Buttons.
	std::vector<std::vector<FieldButton*>> pFieldButtons;
	for(int i = 0; i < m_columns; i++)
	{
		std::vector<FieldButton*> vec_pButtonsTemp;
		for(int j = 0; j < m_rows; j++)
		{
			auto pButton = FieldButton::create( /*elements in sets are unique*/
						   (RandomNums.count(i * m_rows + j) == 1));	
			vec_pButtonsTemp.push_back(pButton);
			
			pFieldGrid->attach(*pButton, i, j, 1, 1);
			
			// Connect to left-click, gets handled like real Button-click.
			pButton->signal_clicked().
			connect(sigc::bind<FieldButton*>
				   (sigc::mem_fun
				   (*this, &GameBox::on_FieldButton_clicked), pButton));
				   
			// Connect to other click-actions, handled manually.
			pButton->signal_button_press_event().
			connect(sigc::bind<FieldButton*>
				   (sigc::mem_fun
				   (*this, &GameBox::on_FieldButton_pressed), pButton));
			
			// Connect hide-event.
			pButton->signal_hide().
			connect(sigc::bind <FieldButton*>
		   		   (sigc::mem_fun
				   (*this, &GameBox::on_hide_FieldButton), pButton));
		}
		pFieldButtons.push_back(vec_pButtonsTemp);
		vec_pButtonsTemp.clear();
	}
	m_pFieldButtons = pFieldButtons;
	
	return pFieldButtons;
}

void GameBox::on_hide_FieldButton(FieldButton* button)
{
	delete button;
}

bool GameBox::on_timeout()
{
	// Let the clock tick.
	if(m_clockruns) ++m_clock;
	
	// Calculate minutes and seconds from clock-counter.
	int seconds = m_clock%60;
	int minutes = m_clock/60;
	
	// Restrict counter to an arbitrary limit.
	if(minutes > 99)
	{
		minutes = 99;
		seconds = 59;
		
		m_clock = 99 * 60 + 59;
	}
	
	// Add leading zero if necessary.
	std::string minutes_str = std::to_string(minutes);
	std::string seconds_str = std::to_string(seconds);
	
	if(minutes_str.length() < 2)
	{
		minutes_str = "0" + minutes_str;
	}
	
	if(seconds_str.length() < 2)
	{
		seconds_str = "0" + seconds_str;
	}
			  
	// Set the Label, also blinking animation
	if(!m_clockruns && m_clock > 0 && !m_pTimerLabel->get_text().empty())
	{
		m_pTimerLabel->set_markup("");
	}
	else
	{
		m_pTimerLabel->set_markup("<big>" + minutes_str + 
								  ":" + seconds_str + "</big>");
	}

	return true;
}


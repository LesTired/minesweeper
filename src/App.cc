/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * App.cc
 * Copyright (C) 2017 Michael Lehner <010.wintermute@gmail.com>
 * 
 * MineSweeper is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MineSweeper is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "App.h"

App::App()
:Gtk::Application("LesTired.GTKmm.MineSweeper", Gio::APPLICATION_HANDLES_OPEN)
{
}


// static
Glib::RefPtr<App> 
App::create()
{
	return Glib::RefPtr<App>(new App());
}


void 
App::on_activate()
{
	try
  	{
    	auto pMainWindow = create_MainWindow();
    	pMainWindow->present();
  	}
  	catch (const Glib::Error& ex)
  	{
    	std::cerr << "App::on_activate(): " << ex.what() << std::endl;
  	}
  	catch (const std::exception& ex)
  	{
    	std::cerr << "App::on_activate(): " << ex.what() << std::endl;
  	}
}


MainWindow* 
App::create_MainWindow()
{
	auto pMainWindow = MainWindow::create();
	
	add_window(*pMainWindow);
	
	pMainWindow->signal_hide().
	connect(sigc::bind <Gtk::Window*>
		   (sigc::mem_fun
		   (*this, &App::on_hide_window), pMainWindow));
	
	return pMainWindow;
}


void 
App::on_hide_window(Gtk::Window* window)
{
	delete window;
}
